package records;

public record CarRecord(String regNumber, String owner) implements I {

    /*
    Records cannot have instance fields
    private final int age;
     */

    public static final String currentYear = "23";

    //custom compact constructor
    public CarRecord {
        if (regNumber.length() <= 4) {
            throw new IllegalArgumentException(regNumber + " is not a valid car registration number");
        }
    }

    //Non-canonical constructor must delegate to another constructor
    public CarRecord() {
        this("     ", "");
    }

    public boolean isNewCar() {
        return regNumber().substring(0, 2).equals(currentYear);
    }

    public static CarRecord createBlankCarRecord() {
        return new CarRecord("     ", "");
    }

    @Override
    public String owner() {
        return owner.toUpperCase();
    }
}

interface I { }
